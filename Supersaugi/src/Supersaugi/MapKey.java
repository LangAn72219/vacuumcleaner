package Supersaugi;

import java.util.Comparator;
import java.util.Objects;

public class MapKey {

    private int x; // x coordinate
    private int y; // y coordinate

    public int getX(){
        return x;
    }

    public int getY() {
        return y;
    }

    public MapKey(double x, double y){

        this.x = (int)Math.round(x/Clean.MAP_CELL_SIZE) * Clean.MAP_CELL_SIZE;
        this.y = (int)Math.round(y/Clean.MAP_CELL_SIZE) * Clean.MAP_CELL_SIZE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapKey that = (MapKey) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public int compare(MapKey a)
    {
        return a.getX();
    }

//    @Override
//    public int compareTo(MapKey o) {
//        return calculateDistance(this.x, o.x, this.y, o.y);
//    }
//
//

}
